import { MainPage } from "./src/MainPage.js"
import { ApiRequest } from "./src/apiRequest.js"

const container = document.getElementById('root')

new MainPage(container, ApiRequest)
