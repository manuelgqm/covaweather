import { MainPage } from '../src/MainPage.js'
import { ApiRequestMock } from './apiRequestMock.js'

describe('Covaweather', ()=> {

    const document = {}
    const apiRequest = new ApiRequestMock
    
    it('the page contains an object', ()=> {
      const mainPage = new MainPage(document, apiRequest)
      const weather = [{
        date: "2020-01-31 15:00:00",
        temp: 20.91,
        pressure: 1021,
        humidity: 48
      }]
      const element = [{
        data: {
          date: "2020-01-31 15:00:00",
          temp: 20.91,
          pressure: 1021,
          humidity: 48
        }
      }]

      mainPage.load(element)

      const result = mainPage.data

      expect(result).toEqual(weather)
    })

    it('the page contains an array of two objects', ()=> {
      const mainPage = new MainPage(document, apiRequest)
      const weather = [
        {
          date: "2020-01-31 15:00:00",
          temp: 20.91,
          pressure: 1021,
          humidity: 48
        },
        {
          date: "2020-02-01 15:00:00",
          temp: 23.85,
          pressure: 1000,
          humidity: 46
        }
      ]

      const element = [
        {
          data: {
            date: "2020-01-31 15:00:00",
            temp: 20.91,
            pressure: 1021,
            humidity: 48
          }
        },
        {
          data: {
            date: "2020-02-01 15:00:00",
            temp: 23.85,
            pressure: 1000,
            humidity: 46
          }
        }
      ]

      mainPage.load(element)

      const result = mainPage.data

      expect(result).toEqual(weather)
    })
})
