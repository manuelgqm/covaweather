from flask import Flask,json
from flask_cors import CORS
import requests
import json
from datetime import datetime

app = Flask(__name__)
CORS(app)

@app.route('/')
def default():
    return 'Hey, we have cross Flask in a Docker container!'

@app.route("/get_forecast")
def get_forecast():
    req = requests.get("http://api.openweathermap.org/data/2.5/forecast?id=6362115&APPID=c79ec39540d8fd087b1cee426dcced35&units=metric&lang=es")
    forecast_dic = json.loads(req.text)
    forecast_list = forecast_dic['list']
    current_forecast_dic = dict(forecast_list[0])
    forecast = {
      'date': current_forecast_dic['dt_txt'],
      'temp': current_forecast_dic['main']['temp'],
      'pressure': current_forecast_dic['main']['pressure'],
      'humidity': current_forecast_dic['main']['humidity']
    }

    jsonStr = json.dumps(forecast)
    return jsonStr

@app.route("/get_forecasts")
def get_forecasts():
  openWeather_forecast = get_openWeather()
  aemet_forecast = get_aemet()
  weathers = [openWeather_forecast, aemet_forecast]
  covaweather_forecast = get_covaweather(weathers)

  result = [
    {
      'service': 'Open Weather',
      'data': openWeather_forecast
    }, {
      'service': 'aemet',
      'data': aemet_forecast
    }, {
      'service': 'COVA',
      'data': covaweather_forecast
    }
  ]

  jsonStr = json.dumps(result)
  return jsonStr

def get_openWeather():
  req = requests.get("http://api.openweathermap.org/data/2.5/forecast?id=6362115&APPID=c79ec39540d8fd087b1cee426dcced35&units=metric&lang=es")
  forecast_dic = json.loads(req.text)
  forecast_list = forecast_dic['list']
  current_forecast_dic = dict(forecast_list[0])
  forecast = {
    'date': current_forecast_dic['dt_txt'],
    'temp': current_forecast_dic['main']['temp'],
    'pressure': current_forecast_dic['main']['pressure'],
    'humidity': current_forecast_dic['main']['humidity']
  }

  return forecast

def get_aemet():
  url = "https://api.tutiempo.net/json/?lan=es&apid=axTaXXqqXXX4sz3&lid=1069"
  api_key = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtYW51ZWxncW1AZ21haWwuY29tIiwianRpIjoiOTExMmMzZmQtN2ViNi00NDBhLTk5ZjgtYTY1YjIzMTM4MDY3IiwiaXNzIjoiQUVNRVQiLCJpYXQiOjE1ODA0Njk1OTIsInVzZXJJZCI6IjkxMTJjM2ZkLTdlYjYtNDQwYS05OWY4LWE2NWIyMzEzODA2NyIsInJvbGUiOiIifQ.fXD8-T0ZVRvI0ZtWj3OphGBHVg_52QYkYR441DVBPls"

  querystring = {"api_key": api_key}

  headers = {
    'cache-control': "no-cache"
  }

  response = requests.request("GET", url, headers=headers, params=querystring)

  forecast_dic = json.loads(response.text)
  current_forecast = forecast_dic["hour_hour"]["hour1"]
  current_date_str = current_forecast["date"] + " " + current_forecast["hour_data"]
  current_date = datetime.strptime(current_date_str, '%Y-%m-%d %H:%M')

  forecast = {
    'date': current_date.strftime('%Y-%m-%d %H:%M:%S') ,
    'temp': current_forecast["temperature"],
    'pressure': current_forecast["pressure"],
    'humidity': current_forecast["humidity"] 
  }
  return forecast

def get_covaweather(weathers):
  forecast = {
    'date': '',
    'temp': None,
    'pressure': None,
    'humidity': None
  }

  total_temp = 0
  total_pressure = 0
  total_humidity = 0

  for weather in weathers:
    total_temp += weather["temp"]
    total_pressure += weather["pressure"]
    total_humidity += weather["humidity"]

  providers_count = len(weathers)

  forecast = {
    'date': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
    'temp': int(total_temp / providers_count),
    'pressure': int(total_pressure / providers_count),
    'humidity': int(total_humidity / providers_count)
  }

  return forecast

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')

